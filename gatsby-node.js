const path = require(`path`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const pageComponent = path.resolve(`./src/components/frame/Page.js`)
  const result = await graphql(
    `
      {
        contentfulWebsite {
          pages {
            slug
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  result.data.contentfulWebsite.pages.forEach(page => {
    createPage({
      path: page.slug,
      component: pageComponent,
      context: {
        slug: page.slug,
      },
    })
  })
}
