# Webclay Website

Built with React, Gatsby, Contentful. To run locally do the following:

npm install  
npm start

To edit the site content you will need an invite to the Webclay organization on Contentful.

### Contentful spaces and environments

In the free tier you can have 2 spaces, with 2 environments per space. So in the Webclay organization we have:

- Prod space - Production environment
- Nonprod space - Staging and Test environments

For example to copy the content & content model from Production to Staging, install the Contentful CLI and do the following:

contentful space export --content-file production.json --skip-roles --skip-webhooks --space-id 9hrjuvvkatd1  
contentful space import --content-file production.json --skip-roles --skip-webhooks --space-id kybm50vfmx9p --environment-id Staging

Use import with care, it will overwrite the existing state! More info here: https://www.contentful.com/developers/docs/tutorials/cli/import-and-export/

### Contentful state backup

An instance of the contentful config is backed up in this repo in contentful-snapshot.json.
