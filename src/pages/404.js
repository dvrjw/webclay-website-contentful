import React from 'react'
import Layout from '../components/frame/Layout'
import SEO from '../components/frame/SEO'

const NotFoundPage = ({ location }) => (
  <Layout location={location}>
    <SEO title="404: Not found" />
    <p style={{ padding: '40px 0' }}>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Layout>
)

export default NotFoundPage
