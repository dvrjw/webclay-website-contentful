import React from 'react'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './FaqBlock.scss'

const FaqBlock = ({faqItems}) => {
    return (
        <Fade>
            <section className='faq-block'>
                <h1>faqs</h1>
                {faqItems.map(faqItem => (
                    <div key={faqItem} className="faq-item">
                        <h3 className="faq-question">
                            {faqItem.question}
                        </h3>
                        <div className="faq-answer">
                            <Markdown contentfulMarkdown={faqItem.answer}/>
                        </div>
                    </div>
                ))}
            </section>
        </Fade>
    )
}

export default FaqBlock
