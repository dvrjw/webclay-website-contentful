import React from 'react'
import Img from 'gatsby-image'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './CaptionedImageBlock.scss'

const CaptionedImageBlock = ({ title, body, images, alignment }) => {
  let className = 'captioned-image-block'
  if (alignment) {
    className += ' ' + alignment
  }
  if (images && images.length > 1) {
    className += ' multi-image image-count-' + images.length
  }
  if (images && images.length > 3) {
    className += ' image-wrap'
  }

  return (
    <Fade>
      <section className={className}>
        <div className="block-text">
          <h2>{title}</h2>
          <div className="body-text">
            <Markdown contentfulMarkdown={body} />
          </div>
        </div>
        {images && (
          <div className="images">
            {images.map((image, i) => (
              <div key={i} className="image-container">
                <div className="image-item">
                  <Img fluid={image.content.fluid} alt={image.content.title} />
                  {image.captionTitle && <div className="caption-title">{image.captionTitle}</div>}
                  {image.captionBody && <Markdown className="caption-body" contentfulMarkdown={image.captionBody} />}
                </div>
              </div>
            ))}
          </div>
        )}
      </section>
    </Fade>
  )
}

export default CaptionedImageBlock
