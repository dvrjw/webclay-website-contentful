import React from 'react'
import Img from 'gatsby-image'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './MainHeaderBlock.scss'
import EmailRegistration from "./EmailRegistration";

const MainHeaderBlock = ({header, text, backgroundImage, emailAddressCta}) => {
    const textHtml = text && text.childMarkdownRemark && text.childMarkdownRemark.html
    const hasLongText = textHtml && textHtml.length > 150
    return (
        <div className={'main-header-block' + (hasLongText ? ' long-text' : '')}>
            <div className="background-image">
                <Img fluid={backgroundImage.fluid} alt={backgroundImage.title} width={2000}/>
            </div>
            <Fade distance="5%" duration={1000}>
                <section>
                    <h1>{header}</h1>
                    <div className="body-text">
                        <Markdown contentfulMarkdown={text}/>
                    </div>
                    {emailAddressCta &&
                    <EmailRegistration emailAddressPlaceholder={emailAddressCta.emailAddressPlaceholder}
                                       buttonText={emailAddressCta.buttonText}
                                       buttonClass='primary'/>
                    }
                </section>
            </Fade>
        </div>
    )
}

export default MainHeaderBlock
