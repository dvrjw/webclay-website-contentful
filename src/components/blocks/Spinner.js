import React from 'react'
import './Spinner.scss'

const Spinner = () => <div className="loading-spinner" />

export default Spinner
