import React from 'react'

const stripOuterParagraph = s => s.replace(/^<p>/, '').replace(/<\/p>\s*$/, '')

const Markdown = ({ contentfulMarkdown, className }) =>
  stripOuterParagraph(contentfulMarkdown.childMarkdownRemark.html)
    .split('\n')
    .map(line => (
      <div
        key={line}
        dangerouslySetInnerHTML={{ __html: line }}
        className={'markdown' + (className ? ' ' + className : '')}
      />
    ))

export default Markdown
