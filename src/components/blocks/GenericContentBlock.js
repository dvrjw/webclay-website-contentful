import React from 'react'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './GenericContentBlock.scss'

const GenericContentBlock = ({text}) => {
    return (
        <Fade>
            <section className='generic-block'>
                <div className="generic-block-text">
                    <Markdown contentfulMarkdown={text}/>
                </div>
            </section>
        </Fade>
    )
}

export default GenericContentBlock
