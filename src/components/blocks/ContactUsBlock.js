import React from 'react'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './ContactUsBlock.scss'

const ContactUsBlock = ({ emailLabel, email, addressLabel, address }) => (
  <div className="contact-us-block">
    <Fade>
      <section>
        <h4>{emailLabel}</h4>
        <a href={'mailto:' + email}>{email}</a>
        <h4>{addressLabel}</h4>
        <div>
          <Markdown contentfulMarkdown={address} />
        </div>
      </section>
    </Fade>
  </div>
)

export default ContactUsBlock
