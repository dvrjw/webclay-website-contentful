import React from 'react'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import Graph from '../../icons/svg/Graph'
import Person from '../../icons/svg/Person'
import Map from '../../icons/svg/Map'
import './IconBlock.scss'

const lookupIcon = icon => {
  switch (icon) {
    case 'Graph':
      return <Graph />
    case 'Person':
      return <Person />
    case 'Map':
      return <Map />
    default:
      return null
  }
}

const IconBlock = ({ title, body, icon, alignment }) => {
  const iconNode = lookupIcon(icon)
  return (
    <div className={'icon-block' + (alignment ? ' ' + alignment : '')}>
      <Fade>
        <section>
          <div className="block-text">
            <h2>{title}</h2>
            <div className="body-text">
              <Markdown contentfulMarkdown={body} />
            </div>
          </div>
          {iconNode && <div className="icon-container">{iconNode}</div>}
        </section>
      </Fade>
    </div>
  )
}

export default IconBlock
