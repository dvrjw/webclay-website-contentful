import React from 'react'
import Fade from '../frame/Fade'
import './EmailRegistration.scss'

const EmailRegistration = ({emailAddressPlaceholder, buttonText, buttonClass}) => {

    return (
        <div className='email-registration'>
            <Fade>
                <form name="email-registration" method="POST" data-netlify="true">
                    <input type="hidden" name="form-name" value="email-registration"/>
                    <div className="input-container">
                        <input
                            placeholder={emailAddressPlaceholder}
                            spellCheck={false}
                            type="email"
                            name="email"
                            required
                        />
                        <button type="submit" className={buttonClass}>
                            <div className="text">{buttonText}</div>
                        </button>
                    </div>
                </form>

            </Fade>
        </div>
    )
};

export default EmailRegistration
