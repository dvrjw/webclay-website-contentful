import React from 'react'
import Markdown from './Markdown'
import './ImageTextBlock.scss'
import Img from 'gatsby-image'

const ImageTextBlock = ({imageTextBlockHeader, imageTextItems}) => {
    return (
        <section className='image-text-block'>
            {imageTextBlockHeader &&
            <div className='image-text-block-header'><Markdown contentfulMarkdown={imageTextBlockHeader}/></div>
            }
            {imageTextItems.map(imageTextItem => (
                <div key={imageTextItem} className="image-text-block-item">

                    {imageTextItem.image &&
                    <div className='image-text-block-image-container'>
                        <Img fixed={imageTextItem.image.fixed}
                             alt={imageTextItem.image.title}/>
                    </div>
                    }

                    <div className='image-text-block-container'>
                        <h3 className="image-text-block-header">
                            {imageTextItem.header}
                        </h3>
                        {imageTextItem.text &&
                        <div className="image-text-block-text">
                            <Markdown contentfulMarkdown={imageTextItem.text}/>
                        </div>
                        }
                    </div>
                </div>
            ))}

        </section>
    )
}

export default ImageTextBlock
