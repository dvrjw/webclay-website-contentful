import React from 'react'
import Fade from '../frame/Fade'
import Markdown from './Markdown'
import './CtaBlock.scss'

const CtaBlock = ({ mdTitle, body }) => (
  <div className="cta-block">
    <Fade>
      <section>
        <h2>
          <Markdown contentfulMarkdown={mdTitle} />
        </h2>
        {body && (
          <div className="body-text">
            <Markdown contentfulMarkdown={body} />
          </div>
        )}
        <div className="coming-soon-message">Coming soon to</div>
      </section>
    </Fade>
  </div>
)

export default CtaBlock
