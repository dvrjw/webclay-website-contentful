import React from 'react'
import Markdown from './Markdown'
import './PricingBlock.scss'
import Img from 'gatsby-image'

const PricingBlock = ({colour, pricingBlockHeader, priceItems}) => {

    return (
        <section className={'pricing-block' + (colour ? ' ' + colour : ' primary')}>
            {pricingBlockHeader &&
            <h2 className="pricing-block-header"><Markdown contentfulMarkdown={pricingBlockHeader}/></h2>
            }
            <div className="pricing-items-wrapper">
                {priceItems.map(priceItem => (
                    <div key={priceItem} className="price-item">
                        <div className='price-item-image'>
                            <Img fixed={priceItem.image.fixed}
                                 alt={priceItem.image.title}/>
                        </div>
                        <h2 className="price-name">
                            {priceItem.name}
                        </h2>
                        {priceItem.headline &&
                        <div classname="price-headline">{priceItem.headline}</div>
                        }
                        <h1 className="price-per-month">
                            ${priceItem.pricePerMonth} / month
                        </h1>
                        <div className="price-description">
                            <Markdown contentfulMarkdown={priceItem.description}/>
                        </div>
                        {priceItem.cta &&
                        <button type="submit" className='link-button dark'>
                            <a href={priceItem.cta.linkUrl} className="text">{priceItem.cta.linkText}</a>
                        </button>
                        }
                    </div>
                ))}
            </div>
        </section>
    )
}

export default PricingBlock
