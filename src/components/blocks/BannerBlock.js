import React from 'react'
import Markdown from './Markdown'
import './BannerBlock.scss'
import EmailRegistration from "./EmailRegistration";

const BannerBlock = ({bannerHeader, text, colour, emailAddressCta, links}) => {
    return (
        <section className={'banner-block' + (colour ? ' ' + colour : ' primary')}>
            {bannerHeader &&
            <div className="banner-header-text">
                <Markdown contentfulMarkdown={bannerHeader}/>
            </div>
            }
            {text &&
            <div className="banner-text">
                <Markdown contentfulMarkdown={text}/>
            </div>
            }
            {emailAddressCta &&
            <EmailRegistration emailAddressPlaceholder={emailAddressCta.emailAddressPlaceholder}
                               buttonText={emailAddressCta.buttonText}
                               buttonClass={colour === 'primary' ? 'dark' : 'light'}/>
            }
            {links && links.map(linkItem => (
                <button type="submit" className='link-button dark'>
                    <a href={linkItem.linkUrl} className="text">{linkItem.linkText}</a>
                </button>
            ))}
        </section>
    )
}

export default BannerBlock
