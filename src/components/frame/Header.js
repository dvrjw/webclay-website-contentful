import React, {useState} from 'react'
import {graphql, Link, useStaticQuery} from 'gatsby'
import Helmet from 'react-helmet'
import MenuButton from './MenuButton'
import './Header.scss'

const isActive = (slug, location) => {
  const path = location.pathname
  if (slug === '/') {
    return !path || path === '/'
  } else {
    return path === slug
  }
}

const NavItems = ({ pages, location, closeMenu, disabled }) =>
  pages.map(page => {
    const active = isActive(page.slug, location)
    return (
      <Link
        key={page.slug}
        to={page.slug}
        className={active ? 'active' : undefined}
        onClick={active && closeMenu ? closeMenu : undefined}
        disabled={disabled}
      >
        {page.title}
      </Link>
    )
  })

const Header = ({ location }) => {
  const { contentfulWebsite } = useStaticQuery(
    graphql`
      {
        contentfulWebsite {
          pages {
            title
            slug
          }
        }
      }
    `
  )

  const [open, setOpen] = useState(false)
  const toggleMenu = () => setOpen(!open)
  const closeMenu = () => setOpen(false)

  return (
    <div className="header-container">
      <Helmet htmlAttributes={{ class: open ? 'menu-open' : undefined, lang: 'en' }} />
      <header>
        <div className="header-background" />
        <div className="header-content">
          <Link to="/" aria-label="Webclay Home">
              <img className="header-logo" src="webclay-logo.png" alt=""/>
              <h3 className="header-title">Webclay</h3>
          </Link>
          <nav className="desktop-nav">
            <NavItems pages={contentfulWebsite.pages} location={location} />
          </nav>
          <MenuButton open={open} onClick={toggleMenu} />
        </div>
      </header>
      <nav className="mobile-nav">
        <NavItems pages={contentfulWebsite.pages} location={location} closeMenu={closeMenu} disabled={!open} />
      </nav>
    </div>
  )
}

export default Header
