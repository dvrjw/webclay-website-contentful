import React from 'react'
import './MenuButton.scss'

const MenuButton = ({ open, onClick }) => (
  <button
    className={'menu-button' + (open ? ' open' : '')}
    onClick={onClick}
    aria-label={open ? 'Close menu' : 'Open menu'}
  >
    <div className="menu-icon">
      <span />
      <span />
      <span />
    </div>
  </button>
)

export default MenuButton
