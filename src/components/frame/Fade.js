import React from 'react'
import Fade from 'react-reveal/Fade'

const CustomFade = ({ children, ...rest }) => (
  <Fade top distance="3%" duration={500} cascade {...rest}>
    {children}
  </Fade>
)

export default CustomFade
