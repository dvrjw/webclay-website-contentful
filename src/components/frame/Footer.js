import React from 'react'
import './Footer.scss'

const Footer = () => (
  <footer>
    <div className="footer-content">
      <div className="footer-copyright">Copyright © {new Date().getFullYear()} Webclay. All rights reserved.</div>
      {/*<div className="footer-links">*/}
      {/*  <span>Follow us</span>*/}
      {/*  <a href="https://www.instagram.com/webclay" rel="noopener noreferrer" target="_blank" aria-label="Webclay Instagram page">*/}
      {/*    <InstagramIcon />*/}
      {/*  </a>*/}
      {/*  <a href="https://www.linkedin.com/company/webclay/" rel="noopener noreferrer" target="_blank" aria-label="Webclay LinkedIn page">*/}
      {/*    <LinkedinIcon />*/}
      {/*  </a>*/}
      {/*  <a href="https://www.facebook.com/webclay" rel="noopener noreferrer" target="_blank" aria-label="Webclay Facebook page">*/}
      {/*    <FacebookIcon />*/}
      {/*  </a>*/}
      {/*</div>*/}
    </div>
  </footer>
)

export default Footer
