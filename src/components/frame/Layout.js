import React from 'react'
import Header from './Header'
import Footer from './Footer'
import './BaseStyles.scss'
import './Layout.scss'

const Layout = ({ children, location, title }) => (
  <div className={'layout' + (title ? ' ' + title.toLowerCase() : '')}>
    <Header location={location} />
    <main>{children}</main>
    <Footer />
  </div>
)

export default Layout
