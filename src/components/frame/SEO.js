import React from 'react'
import Helmet from 'react-helmet'
import {graphql, useStaticQuery} from 'gatsby'

const SEO = ({ pageTitle }) => {
    const {
    contentfulWebsite: { websiteTitle, websiteDescription },
  } = useStaticQuery(
    graphql`
      {
        contentfulWebsite {
          websiteTitle
          websiteDescription
        }
      } 
    `
  )

  // const image = 'https://webclaywebsite.netlify.com/logo.png' TODO ?????
  return (
    <Helmet
      title={websiteDescription}
      titleTemplate={`%s | ${websiteTitle}`}
      meta={[
        {
          name: 'description',
          content: websiteDescription,
        },
        // {
        //   name: 'image',
        //   content: image,
        // },
        {
          property: 'og:title',
          content: websiteTitle,
        },
        {
          property: 'og:description',
          content: websiteDescription,
        },
        // {
        //   name: 'og:image',
        //   content: image,
        // },
        {
          property: 'og:type',
          content: 'website',
        },
        {
          name: 'twitter:description',
          content: websiteDescription,
        },
        {
          name: 'twitter:card',
          content: 'summary',
        },
        {
          name: 'twitter:creator',
          content: 'Webclay',
        },
        {
          name: 'twitter:title',
          content: websiteTitle,
        },
      ]}
    />
  )
}

export default SEO
