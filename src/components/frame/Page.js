import React from 'react'
import { graphql } from 'gatsby'
import { setBlockColours } from '../../utils/setBlockColours'
import Layout from './Layout'
import SEO from './SEO'
import MainHeaderBlock from '../blocks/MainHeaderBlock'
import BannerBlock from '../blocks/BannerBlock'
import CtaBlock from '../blocks/CtaBlock'
import EmailRegistration from '../blocks/EmailRegistration'
import IconBlock from '../blocks/IconBlock'
import ContactUsBlock from '../blocks/ContactUsBlock'
import GenericContentBlock from "../blocks/GenericContentBlock";
import FaqBlock from "../blocks/FaqBlock";
import ImageTextBlock from "../blocks/ImageTextBlock";
import PricingBlock from "../blocks/PricingBlock"

// --- TODO: Put this inside ContentfulCaptionedImageBlock once we have at least one captioned image.
// images {
//   title
//   content {
//     fluid {
//       ...GatsbyContentfulFluid_noBase64
//     }
//   }
//   captionTitle
//   captionBody {
//     childMarkdownRemark {
//       html
//     }
//   }
// }

export const pageQuery = graphql`
  query PageBySlug($slug: String!) {
    contentfulPage(slug: { eq: $slug }) {
      title
      slug
      blocks {
        __typename
        ... on ContentfulMainHeaderBlock {
          id
          header
          text {
            childMarkdownRemark {
              html
            }
          }
          backgroundImage {
            title
            fluid(maxWidth: 1920, maxHeight: 768, quality: 100) {
              ...GatsbyContentfulFluid_tracedSVG
            }
          }
          emailAddressCta {
            emailAddressPlaceholder
            buttonText
          }          
        }
        ... on ContentfulBannerBlock {
          bannerHeader : header {
            childMarkdownRemark {
              html
            }
          }
          text {
            childMarkdownRemark {
              html
            }
          }
          emailAddressCta {
            emailAddressPlaceholder
            buttonText
          }  
          links {
            linkText
            linkUrl
          }
        }
        ... on ContentfulImageTextContainerBlock {
          imageTextBlockHeader : header {
            childMarkdownRemark {
              html
            }          
          }
          imageTextItems {
            header
            text {
              childMarkdownRemark {
                html
              }
            }
            image {
              fixed(width: 130, height: 130, quality: 100) {
                 ...GatsbyContentfulFixed
              }
            }
          }
        }
        ... on ContentfulFaqBlock {
          faqItems {
            question
            answer {
              childMarkdownRemark {
                html
              }
            }
          }
        }        
        ... on ContentfulPricingBlock {
          pricingBlockHeader : header {
            childMarkdownRemark {
              html
            }          
          }
          priceItems {
            name
            headline
            pricePerMonth
            description {
              childMarkdownRemark {
                html
              }          
            }
            image {
              fixed(width: 130, height: 130, quality: 100) {
                 ...GatsbyContentfulFixed
              }
            }
            cta {
              linkText
              linkUrl
            }
          }
        }
      }
    }
  }
`

const Page = ({ data, location }) => {
  const page = data.contentfulPage
  console.log('page', page);
  return (
    <Layout location={location} title={page.title}>
      <SEO pageTitle={page.title} />
      {page.blocks &&
        setBlockColours(page.blocks).map(block => {
          console.log('block', block);
          switch (block.__typename) {
            case 'ContentfulMainHeaderBlock':
              return <MainHeaderBlock key={block.id} {...block} />
            case 'ContentfulBannerBlock':
              return <BannerBlock key={block.id} {...block} />
            case 'ContentfulGenericContentBlock':
              return <GenericContentBlock key={block.id} {...block} />
            case 'ContentfulImageTextContainerBlock':
              return <ImageTextBlock key={block.id} {...block} />
            case 'ContentfulFaqBlock':
              return <FaqBlock key={block.id} {...block} />
            case 'ContentfulCtaBlock':
              return <CtaBlock key={block.id} {...block} />
            case 'ContentfulEmailRegistrationBlock':
              return <EmailRegistration key={block.id} {...block} />
            case 'ContentfulIconBlock':
              return <IconBlock key={block.id} {...block} />
            case 'ContentfulContactUsBlock':
              return <ContactUsBlock key={block.id} {...block} />
            case 'ContentfulPricingBlock':
              return <PricingBlock key={block.id} {...block} />
            default:
              return null
          }
        })}
    </Layout>
  )
}

export default Page
