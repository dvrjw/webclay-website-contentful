// Returns a new array of blocks with an additional colour value
export const setBlockColours = blocks =>
    blocks.reduce((newBlocks, block, i) => {
        const doesChangeColour = block.__typename === 'ContentfulBannerBlock' || block.__typename === 'ContentfulPricingBlock'
        let colour = null;
        if (doesChangeColour) {
            const lastBlockColour = newBlocks.filter((b, j) => j < i && b.colour).reverse()[0]
            if (lastBlockColour) {
                colour = lastBlockColour.colour === 'primary' ? 'secondary' : 'primary'
            } else {
                colour = 'primary'
            }
        }
        newBlocks.push({...block, colour})
        return newBlocks
    }, [])
