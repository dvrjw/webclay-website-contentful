module.exports = {
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-source-contentful',
      options: {
        spaceId: 'cti7snaqosbm',
        accessToken: 'RVcuhFz4a1hZr0tm2HliP33_Yf7zCqR2GPFwDkMJUIQ', // Delivery API (read) access token
        // downloadLocal: true,
        // environment: 'Production',
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: ['Poppins:400,500,600,700', 'Work Sans:400,500,600,700'],
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'webclay-website',
        short_name: 'webclay',
        start_url: '/',
        background_color: 'black',
        theme_color: 'black',
        display: 'minimal-ui',
        icon: 'src/icons/favicon.png',
      },
    },
    'gatsby-transformer-remark',
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // 'gatsby-plugin-offline',
  ],
}
